//////////////////////////////////////////////////////
//  DroneCVIARC14ROSModule_KeypointsNode.h
//
//  Created on: 29 Feb, 2016
//      Author: Hriday
//
//
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>

#include <string>


// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"



//ROSModule
#include "droneRobotLocalizationROSModule.h"


//Communication Definition -> Deprecated!
//#include "communication_definition.h"

//Nodes names -> Deprecated!
//MODULE_NAME_DRONE_KEYPOINTS_GRID_DETECTOR
//#include "nodes_definition.h"




using namespace std;

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, "droneRobotLocationROSModuleNode"); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n; //Este nodo admite argumentos!!

    std::string node_name=ros::this_node::getName();
    cout<<"node name="<<node_name<<endl;

    //Class definition
    droneRobotLocalizationROSModule MyDroneRobotLocalizationROSModule;

    //Open!
    MyDroneRobotLocalizationROSModule.open(n);

    //    droneBebopAttitudeSub.subscribe(n,"states/ardrone3/PilotingState/AttitudeChanged",1);
    //    droneBebopAltitudeSub.subscribe(n,"states/ardrone3/PilotingState/AltitudeChanged",1);
    //    droneBebopSpeedsSub.subscribe(n,"states/ardrone3/PilotingState/SpeedChanged",1);


    //Loop -> Ashyncronous Module
    //while(ros::ok())
    {
        ros::spin();
    }

    return 1;
}

