//////////////////////////////////////////////////////
//  droneExamplePackageROSModuleSource.cpp
//
//  Created on: 29 Feb, 2016
//      Author: Hriday
//
//////////////////////////////////////////////////////


#include "droneRobotLocalizationROSModule.h"


// using namespaces only in cpp files!!
using namespace std;



droneRobotLocalizationROSModule::droneRobotLocalizationROSModule() : DroneModule(droneModule::active),
    sync (SyncPolicy(1))
{

    return;
}


droneRobotLocalizationROSModule::~droneRobotLocalizationROSModule()
{
    close();
    return;
}

void droneRobotLocalizationROSModule::readParameters()
{
    //Config file
    ros::param::get("~config_file", configFile);
    if ( configFile.length() == 0)
    {
        configFile="robot_localization.xml";
    }
}

bool droneRobotLocalizationROSModule::init(std::string configFile)
{
    readConfigs(configFile);
    DroneModule::init();

    filtered_derivative_wcb_x.setTimeParameters( 0.005,0.005,0.200,1.0,100.000);
    filtered_derivative_wcb_y.setTimeParameters( 0.005,0.005,0.200,1.0,100.000);
    filtered_derivative_wcb_z.setTimeParameters( 0.005,0.005,0.200,1.0,100.000);

    filtered_derivative_wcb_x.reset();
    filtered_derivative_wcb_y.reset();
    filtered_derivative_wcb_z.reset();

    filtered_derivative_wcb_lx.setTimeParameters( 0.005,0.005,0.200,1.0,100.000);
    filtered_derivative_wcb_ly.setTimeParameters( 0.005,0.005,0.200,1.0,100.000);

    filtered_derivative_wcb_lx.reset();
    filtered_derivative_wcb_ly.reset();

    filtered_derivative_wcb_lwx.setTimeParameters( 0.005,0.005,0.200,1.0,100.000);
    filtered_derivative_wcb_lwy.setTimeParameters( 0.005,0.005,0.200,1.0,100.000);

    filtered_derivative_wcb_lwx.reset();
    filtered_derivative_wcb_lwy.reset();


    filtered_derivative_vos_x.setTimeParameters( 0.05,0.05,0.200,1.0,10.000);
    filtered_derivative_vos_y.setTimeParameters( 0.05,0.05,0.200,1.0,10.000);
    filtered_derivative_vos_z.setTimeParameters( 0.05,0.05,0.200,1.0,10.000);

    filtered_derivative_vos_x.reset();
    filtered_derivative_vos_y.reset();
    filtered_derivative_vos_z.reset();

    bebop_first_yaw_measurement_ = false;

    return true;
}

bool droneRobotLocalizationROSModule::readConfigs(std::string configFile)
{

    try
    {
        XMLFileReader my_xml_reader(configFile);

        init_position_x   = my_xml_reader.readDoubleValue("take_off_site:position:x");
        init_position_y   = my_xml_reader.readDoubleValue("take_off_site:position:y");
        init_position_z   = my_xml_reader.readDoubleValue("take_off_site:position:z");

        init_yaw          = my_xml_reader.readDoubleValue("take_off_site:attitude:yaw");
        init_roll         = my_xml_reader.readDoubleValue("take_off_site:attitude:roll");
        init_pitch        = my_xml_reader.readDoubleValue("take_off_site:attitude:pitch");

        //Converting degrees to radians
        init_yaw    = init_yaw*(M_PI/180.0);
        init_roll   = init_roll*(M_PI/180.0);
        init_pitch  = init_pitch*(M_PI/180.0);

        altitude_co_z     = my_xml_reader.readDoubleValue("covariances:altitude:z");
        altitude_co_dz    = my_xml_reader.readDoubleValue("covariances:altitude:dz");


        optical_flow_co_x = my_xml_reader.readDoubleValue("covariances:optical_flow:dx");
        optical_flow_co_y = my_xml_reader.readDoubleValue("covariances:optical_flow:dy");

        pitch_cmd_co      = my_xml_reader.readDoubleValue("covariances:controller_odom:pitch_command");
        roll_cmd_co       = my_xml_reader.readDoubleValue("covariances:controller_odom:roll_command");
        daltitude_cmd_co  = my_xml_reader.readDoubleValue("covariances:controller_odom:daltitude_command");
        dyaw_cmd_co       = my_xml_reader.readDoubleValue("covariances:controller_odom:dyaw_command");
    }


    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }


    return true;
}


void droneRobotLocalizationROSModule::open(ros::NodeHandle & nIn)
{
    //Node
    DroneModule::open(nIn);

    //read the parameters from the launch file
    readParameters();
    //Init
    if(!init(stackPath+"config/drone"+cvg_int_to_string(idDrone)+"/"+configFile))
    {
        cout<<"Error init"<<endl;
        return;
    }



    //// TOPICS
    //Subscribers
    droneImuSub                 = n.subscribe("imu", 1, &droneRobotLocalizationROSModule::droneImuCallback, this);
    droneCommandPitchRollSub    = n.subscribe("command/pitch_roll",1,&droneRobotLocalizationROSModule::droneCommandPitchRollCallback, this);
    droneCommandDaltitudeSub    = n.subscribe("command/dAltitude",1,&droneRobotLocalizationROSModule::droneCommandDaltitudeCallback, this);
    droneCommandDyawSub         = n.subscribe("command/dYaw",1, &droneRobotLocalizationROSModule::droneCommandDyawCallback, this);
    droneAltitudeSub            = n.subscribe("altitude",1,&droneRobotLocalizationROSModule::droneAltitudeCallback, this);
    droneSpeedsSub              = n.subscribe("ground_speed",1,&droneRobotLocalizationROSModule::droneSpeedsCallback, this);
    droneOdomFilteredSub        = n.subscribe("odometry/filtered",1,&droneRobotLocalizationROSModule::droneOdometryFilteredCallback, this);
    droneRotationAnglesSub      = n.subscribe("rotation_angles",1,&droneRobotLocalizationROSModule::droneRotationAnglesCallback, this);
    dronePnPPoseSub             = n.subscribe("vb_estimated_pose/rpnp_pose",1,&droneRobotLocalizationROSModule::dronePnPPoseCallback, this);
    droneGPSDataSub             = n.subscribe("mavros/global_position/raw/fix",1,&droneRobotLocalizationROSModule::droneGPSDataCallback, this);
    droneHectorSlamdPoseSub     = n.subscribe("poseupdate",1,&droneRobotLocalizationROSModule::droneHectorSlamPoseCallback, this);
    mavrosLocalSpeedsSubsriber  = n.subscribe("/gazebo/model_states",1, &droneRobotLocalizationROSModule::localSpeedsCallbackGazebo, this);
    droneLaserScanPoseSub       = n.subscribe("lb_estimated_pose/door_pose", 1, &droneRobotLocalizationROSModule::droneLaserScanPoseCallback, this);
    droneLaserScanWindowPoseSub = n.subscribe("lb_estimated_pose/window_pose", 1, &droneRobotLocalizationROSModule::droneLaserScanWindowPoseCallback, this);
    droneSlamDunkPoseSub        = n.subscribe("/pose", 1, &droneRobotLocalizationROSModule::droneSlamDunkPoseCallback, this);
    droneVOSematicSLAMSub       = n.subscribe("/final_pose",1, &droneRobotLocalizationROSModule::droneVOSemanticSLAMPoseCallback, this);
    droneBebopOdomSub           = n.subscribe("odom",1, &droneRobotLocalizationROSModule::droneBebopOdomCallback, this);

    //time synchronization of bebop messages
    droneBebopAttitudeSub.subscribe(n,"states/ardrone3/PilotingState/AttitudeChanged",1);
    droneBebopAltitudeSub.subscribe(n,"states/ardrone3/PilotingState/AltitudeChanged",1);
    droneBebopSpeedsSub.subscribe(n,"states/ardrone3/PilotingState/SpeedChanged",1);
    sync.connectInput(droneBebopAttitudeSub,droneBebopAltitudeSub,droneBebopSpeedsSub);
    sync.registerCallback(boost::bind(&droneRobotLocalizationROSModule::droneBebopCallback,this, _1, _2, _3));

    //Publishers
    droneImuPub             = n.advertise<sensor_msgs::Imu>("imu/data", 1, true);
    droneRotationAnglesPub  = n.advertise<sensor_msgs::Imu>("rotation_angles/data",1,true);
    droneOdomPub            = n.advertise<nav_msgs::Odometry>("controller/odom/data",1,true);
    droneAltitudePub        = n.advertise<nav_msgs::Odometry>("altitude/data",1,true);
    droneAltitudeGTPub        = n.advertise<nav_msgs::Odometry>("altitude_gt/data",1,true);
    droneGroundSpeedsPub    = n.advertise<nav_msgs::Odometry>("optical_flow/data",1,true);
    dronePnPPosePub         = n.advertise<nav_msgs::Odometry>("PnP_pose/data",1, true);
    droneGPSdataPub         = n.advertise<sensor_msgs::NavSatFix>("gps/fix_data",1, true);
    droneHectorSlamDataPub  = n.advertise<nav_msgs::Odometry>("hector_slam/data",1,true);
    droneLaserScanPosePub   = n.advertise<nav_msgs::Odometry>("laser_scan_pose/data",1,true);
    droneLaserScanWindowPosePub   = n.advertise<nav_msgs::Odometry>("laser_scan_window_pose/data",1,true);
    droneSetModePub         = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("set_pose",1,true);
    droneEstimatedPosePub   = n.advertise<droneMsgsROS::dronePose>("EstimatedPose_droneGMR_wrt_GFF",1,true);
    droneEstimatedSpeedsPub = n.advertise<droneMsgsROS::droneSpeeds>("EstimatedSpeed_droneGMR_wrt_GFF",1,true);
    droneSlamDunkPosePub    = n.advertise<nav_msgs::Odometry>("slamdunk_pose/data", 1, true);
    droneVOSematicSLAMPub   = n.advertise<nav_msgs::Odometry>("VO_Semantic_SLAM/Pose/data",1, true);
    droneBebopIMUPub        = n.advertise<sensor_msgs::Imu>("bebop_imu/data", 1, true);
    dronebebopVelandAltiPub = n.advertise<nav_msgs::Odometry>("bebop_vel_alti/data",1, true);
    droneBebopOdomDataPub   = n.advertise<nav_msgs::Odometry>("bebop_odom/data",1,true);

    //Flag of module opened
    droneModuleOpened=true;

    //autostart module!
    moduleStarted=false;

    //End
    return;
}


void droneRobotLocalizationROSModule::close()
{
    DroneModule::close();

    //Do stuff

    return;
}


bool droneRobotLocalizationROSModule::resetValues()
{
    //Reseting the ekf

    ResetPose.header.stamp = ros::Time::now();
    ResetPose.header.frame_id = "speeds_odom";
    ResetPose.pose.pose.position.x    = init_position_x;
    ResetPose.pose.pose.position.y    = init_position_y;
    ResetPose.pose.pose.position.z    = init_position_z;

    tf::Quaternion quaternion = tf::createQuaternionFromRPY(init_roll,init_pitch,init_yaw);

    ResetPose.pose.pose.orientation.x = quaternion.getX();
    ResetPose.pose.pose.orientation.y = quaternion.getY();
    ResetPose.pose.pose.orientation.z = quaternion.getZ();
    ResetPose.pose.pose.orientation.w = quaternion.getW();

    for (size_t ind = 0; ind < 36; ind+=7)
    {
        ResetPose.pose.covariance[ind] = 1e-6;
    }

    droneSetModePub.publish(ResetPose);
    return true;
}


bool droneRobotLocalizationROSModule::startVal()
{
    //Reseting the ekf

    ResetPose.header.stamp = ros::Time::now();
    ResetPose.header.frame_id = "speeds_odom";
    ResetPose.pose.pose.position.x    = init_position_x;
    ResetPose.pose.pose.position.y    = init_position_y;
    ResetPose.pose.pose.position.z    = init_position_z;

    tf::Quaternion quaternion = tf::createQuaternionFromRPY(init_roll,init_pitch,init_yaw);

    ResetPose.pose.pose.orientation.x = quaternion.getX();
    ResetPose.pose.pose.orientation.y = quaternion.getY();
    ResetPose.pose.pose.orientation.z = quaternion.getZ();
    ResetPose.pose.pose.orientation.w = quaternion.getW();

    for (size_t ind = 0; ind < 36; ind+=7)
    {
        ResetPose.pose.covariance[ind] = 1e-6;
    }

    droneSetModePub.publish(ResetPose);

    //End
    return DroneModule::startVal();
}


bool droneRobotLocalizationROSModule::stopVal()
{
    //Do stuff

    return DroneModule::stopVal();
}


bool droneRobotLocalizationROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    return true;
}

void droneRobotLocalizationROSModule::droneImuCallback(const sensor_msgs::Imu &msg)
{
    //Publishing IMU data to the EKF
    Imu_data.header.stamp = msg.header.stamp;
    Imu_data.header.frame_id = "fcu";

    Imu_data.orientation            = msg.orientation;
    Imu_data.orientation_covariance = msg.orientation_covariance;
    Imu_data.orientation_covariance[0] = 0.01;
    Imu_data.orientation_covariance[1] = 0.0;
    Imu_data.orientation_covariance[2] = 0.0;
    Imu_data.orientation_covariance[3] = 0.0;
    Imu_data.orientation_covariance[4] = 0.01;
    Imu_data.orientation_covariance[5] = 0.0;
    Imu_data.orientation_covariance[6] = 0.0;
    Imu_data.orientation_covariance[7] = 0.0;
    Imu_data.orientation_covariance[8]=  0.01;
    
    //converting from NED frame to ENU frame for the robot_localization
    Imu_data.angular_velocity.x = +1*msg.angular_velocity.x;
    Imu_data.angular_velocity.y = -1*msg.angular_velocity.y;
    Imu_data.angular_velocity.z = -1*msg.angular_velocity.z;
    Imu_data.angular_velocity_covariance = msg.angular_velocity_covariance;
    
    //converting from NED frame to ENU frame for the robot_localization
    Imu_data.linear_acceleration.x = +1*msg.linear_acceleration.x;
    Imu_data.linear_acceleration.y = -1*msg.linear_acceleration.y;
    Imu_data.linear_acceleration.z = -1*msg.linear_acceleration.z;

    PublishImuData();
    return;
}

//This data is Published only if the drone does not publish IMU data
void droneRobotLocalizationROSModule::droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped &msg)
{
    rotationAngleData.header.stamp    = ros::Time::now();
    rotationAngleData.header.frame_id = "fcu";
    //rotationAngleData.child_frame_id  = "fcu";

    //convert from degrees to radians
    roll_rad      =  (msg.vector.x) * (M_PI/180);
    pitch_rad     = -(msg.vector.y) * (M_PI/180);
    yaw_rad       = -(msg.vector.z) * (M_PI/180);

    tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll_rad, pitch_rad, yaw_rad);

    rotationAngleData.orientation.x = quaternion.getX();
    rotationAngleData.orientation.y = quaternion.getY();
    rotationAngleData.orientation.z = quaternion.getZ();
    rotationAngleData.orientation.w = quaternion.getW();

    rotationAngleData.orientation_covariance[0] = 0.01;
    rotationAngleData.orientation_covariance[4] = 0.01;
    rotationAngleData.orientation_covariance[8] = 0.01;


    //    rotationAngleData.pose.pose.orientation.x = quaternion.getX();
    //    rotationAngleData.pose.pose.orientation.y = quaternion.getY();
    //    rotationAngleData.pose.pose.orientation.z = quaternion.getZ();
    //    rotationAngleData.pose.pose.orientation.w = quaternion.getW();
    //    rotationAngleData.pose.covariance[21] = 0.33;
    //    rotationAngleData.pose.covariance[28] = 0.33;
    //    rotationAngleData.pose.covariance[35] = 0.33;

    PublishRotationAnglesData();
}

void droneRobotLocalizationROSModule::droneCommandPitchRollCallback(const droneMsgsROS::dronePitchRollCmd &msg)
{
    PitchRollCmd.pitchCmd = msg.pitchCmd;
    PitchRollCmd.rollCmd  = msg.rollCmd;
    receivedPitchRoll = true;
    return;
}

void droneRobotLocalizationROSModule::droneCommandDaltitudeCallback(const droneMsgsROS::droneDAltitudeCmd &msg)
{
    DaltitudeCmd.dAltitudeCmd = msg.dAltitudeCmd;
    receivedDaltitude = true;
    return;
}

void droneRobotLocalizationROSModule::droneCommandDyawCallback(const droneMsgsROS::droneDYawCmd &msg)
{
    DyawCmd.dYawCmd = msg.dYawCmd;
    receivedDyaw = true;

    PublishOdomData();
    return;
}

void droneRobotLocalizationROSModule::droneAltitudeCallback(const droneMsgsROS::droneAltitude &msg)
{
    //Publishing Alitude data to the EKF
    altitudeData.header.stamp    = msg.header.stamp;
    altitudeData.header.frame_id = "speeds_odom";
    altitudeData.child_frame_id  = "fcu";

    altitudeData.pose.pose.position.z = -msg.altitude;
    altitudeData.twist.twist.linear.z = -msg.altitude_speed;
    altitudeData.pose.covariance[14]  = altitude_co_z;
    altitudeData.twist.covariance[14] = altitude_co_dz;


    PublishAltitudeData();
    return;
}

void droneRobotLocalizationROSModule::droneSpeedsCallback(const droneMsgsROS::vector2Stamped &msg)
{
    //Publishing optical flow data to the EKF
    groundSpeedsData.header.stamp = ros::Time::now();
    groundSpeedsData.header.frame_id = "speeds_odom";
    groundSpeedsData.child_frame_id  = "fcu";

    groundSpeedsData.twist.twist.linear.x  =  msg.vector.x;
    groundSpeedsData.twist.twist.linear.y  = -msg.vector.y;
    groundSpeedsData.twist.covariance[0]   = optical_flow_co_x;
    groundSpeedsData.twist.covariance[7]   = optical_flow_co_y;

    PublishSpeedsData();
    return;
}

void droneRobotLocalizationROSModule::dronePnPPoseCallback(const geometry_msgs::Pose &msg)
{
    PnPPoseData.header.stamp = ros::Time::now();
    PnPPoseData.child_frame_id  = "fcu";
    PnPPoseData.header.frame_id = "speeds_odom";

    PnPPoseData.pose.pose.position.x = msg.position.x;
    PnPPoseData.pose.pose.position.y = msg.position.y;
    PnPPoseData.pose.pose.position.z = msg.position.z;

    //std::cout << EstimatedPose.x << std::endl;
    if(EstimatedPose.x >= 6.0 && EstimatedPose.x <= 10.0){
        PnPPoseData.pose.covariance[0]  = 1.0;
        PnPPoseData.pose.covariance[7]  = 1.0;
        PnPPoseData.pose.covariance[14] = 1.0;
    }
    else{
        PnPPoseData.pose.covariance[0]  = 10000.0;
        PnPPoseData.pose.covariance[7]  = 10000.0;
        PnPPoseData.pose.covariance[14] = 10000.0;
    }

    PublishPnPData();

}

void droneRobotLocalizationROSModule::droneGPSDataCallback(const sensor_msgs::NavSatFix &msg)
{
    gps_data.header.stamp             = msg.header.stamp;
    gps_data.header.frame_id          = "gps";
    gps_data.latitude                 = msg.latitude;
    gps_data.longitude                = msg.longitude;
    gps_data.altitude                 = msg.altitude;
    gps_data.position_covariance[0]   = 100;
    gps_data.position_covariance[4]   = 100;
    gps_data.position_covariance[8]   = 100;
    gps_data.position_covariance_type = 1;

    PublishGPSData();
}


void droneRobotLocalizationROSModule::localSpeedsCallbackGazebo(const gazebo_msgs::ModelStatesConstPtr &msg){

    /* Calculating Roll, Pitch, Yaw */
    tf::Quaternion q(msg->pose[2].orientation.x, msg->pose[2].orientation.y, msg->pose[2].orientation.z, msg->pose[2].orientation.w);
    tf::Matrix3x3 m(q);

    //convert quaternion to euler angels
    double y, p, r;
    m.getEulerYPR(y, p, r);

    yaw_ground_truth = y;

    // Publish ground truth altitude data
    altitudeGTData.header.stamp    = ros::Time::now();
    altitudeGTData.header.frame_id = "speeds_odom";
    altitudeGTData.child_frame_id  = "fcu";

    altitudeGTData.pose.pose.position.z = msg->pose[2].position.z;
    altitudeGTData.twist.twist.linear.z = msg->twist[2].linear.z;
    altitudeGTData.pose.covariance[14]  = altitude_co_z;
    altitudeGTData.twist.covariance[14] = altitude_co_dz;

    PublishAltitudeGTData();

}

void droneRobotLocalizationROSModule::droneHectorSlamPoseCallback(const geometry_msgs::PoseWithCovarianceStamped &msg)
{
    HectorSlamPoseData.child_frame_id  = "fcu";
    HectorSlamPoseData.header.frame_id = "speeds_odom";
    HectorSlamPoseData.header.stamp    = msg.header.stamp;

    //     geometry_msgs::PoseStamped PoseIn;
    //     geometry_msgs::PoseStamped PoseOut;

    //     PoseIn.header.stamp    = msg.header.stamp;
    //     PoseIn.header.frame_id = "/map";
    //     PoseIn.pose            = msg.pose;

    //     try{
    //     listener.transformPose("/odom",ros::Time(0) ,PoseIn, "/map",PoseOut);
    //     std::cout << "PoseIn" <<  PoseIn << std::endl;
    //     std::cout << "PoseOut" << PoseOut << std::endl;
    //     }
    //     catch (tf::TransformException ex)
    //     {
    //         ROS_ERROR("%s",ex.what());
    //         ros::Duration(1.0).sleep();
    //     }

    //     geometry_msgs::PoseStamped PoseDiff;
    //     PoseDiff.pose.position.x = PoseIn.pose.position.x - PoseOut.pose.position.x;
    //     PoseDiff.pose.position.y = PoseIn.pose.position.y - PoseOut.pose.position.y;
    //     std::cout << "PoseDiff " << PoseDiff << std::endl;


    ros::Time current_timestamp = ros::Time::now();

    double x_raw_t = msg.pose.pose.position.x;
    double y_raw_t = msg.pose.pose.position.y;


    time_t tv_sec; suseconds_t tv_usec;
    {
        tv_sec  = current_timestamp.sec;
        tv_usec = current_timestamp.nsec / 1000.0;
        filtered_derivative_wcb_x.setInput( x_raw_t, tv_sec, tv_usec);
        filtered_derivative_wcb_y.setInput( y_raw_t, tv_sec, tv_usec);
    }

    double x_t, dx_t;
    double y_t, dy_t;
    filtered_derivative_wcb_x.getOutput( x_t,  dx_t);
    filtered_derivative_wcb_y.getOutput( y_t,  dy_t);


    //     std::cout << "x_raw_t" << x_raw_t << std::endl;
    //     std::cout << "x_t" << x_t << std::endl;
    //     std::cout << "dx_t " << dx_t << std::endl;
    //     std::cout << "dy_t " << dy_t << std::endl;

    HectorSlamPoseData.pose.pose.position.x = msg.pose.pose.position.x ;  /*PoseIn.pose.position.x */;
    HectorSlamPoseData.pose.pose.position.y = msg.pose.pose.position.y ;  /*PoseIn.pose.position.y*/;
    HectorSlamPoseData.pose.pose.orientation = msg.pose.pose.orientation;

    // Converting to Body
    /* Calculating Roll, Pitch, Yaw */
    //     tf::Quaternion q(msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w);
    //     tf::Matrix3x3 m(q);

    //convert quaternion to euler angels
    double y, p, r;
    //     m.getEulerYPR(y, p, r);
    y = EstimatedPose.yaw;
    p = EstimatedPose.pitch;
    r = EstimatedPose.roll;

    Eigen::Vector3f BodyFrame;
    Eigen::Vector3f GlobalFrame;
    Eigen::Matrix3f RotationMat;

    GlobalFrame(0) = (+1)*dx_t;
    GlobalFrame(1) = (+1)*dy_t;
    GlobalFrame(2) = 0;

    RotationMat(0,0) = cos(y);
    RotationMat(1,0) = -sin(y);
    RotationMat(2,0) = 0;

    RotationMat(0,1) = sin(y);
    RotationMat(1,1) = cos(y);
    RotationMat(2,1) = 0;

    RotationMat(0,2) = 0;
    RotationMat(1,2) = 0;
    RotationMat(2,2) = 1;

    BodyFrame = RotationMat*GlobalFrame;


    HectorSlamPoseData.twist.twist.linear.x  = (+1) * BodyFrame(0);
    HectorSlamPoseData.twist.twist.linear.y  = (+1) * BodyFrame(1);

    //     HectorSlamPoseData.twist.twist.linear.x  = dx_t;
    //     HectorSlamPoseData.twist.twist.linear.y  = dy_t;


    //     geometry_msgs::PoseStamped PoseDiff;
    //     PoseDiff.pose.position.x = PoseIn.pose.position.x - PoseOut.pose.position.x;
    //     PoseDiff.pose.position.y = PoseIn.pose.position.y - PoseOut.pose.position.y;
    //     std::cout << "PoseDiff " << PoseDiff << std::endl;

    //      if(HectorSlamPoseData.pose.pose.position.x  > -20 && HectorSlamPoseData.pose.pose.position.x < 20 &&
    //        HectorSlamPoseData.pose.pose.position.y  > -20 && HectorSlamPoseData.pose.pose.position.y < 20)
    //     {
    //HectorSlamPoseData.pose.covariance[0]   = 0.0099;
    //HectorSlamPoseData.pose.covariance[7]   = 0.0099;
    //HectorSlamPoseData.pose.covariance[28]  = 0.0099;
    //HectorSlamPoseData.pose.covariance[35]  = 0.0099;
    //HectorSlamPoseData.twist.covariance[0]  = 0.0099;
    //HectorSlamPoseData.twist.covariance[7]  = 0.0099;

    HectorSlamPoseData.pose.covariance[0]   = 0.9;
    HectorSlamPoseData.pose.covariance[7]   = 0.9;
    HectorSlamPoseData.pose.covariance[28]  = 0.9;
    HectorSlamPoseData.pose.covariance[35]  = 0.9;
    HectorSlamPoseData.twist.covariance[0]  = 0.9;
    HectorSlamPoseData.twist.covariance[7]  = 0.9;
    //     }
    //     else
    //     {
    //     HectorSlamPoseData.pose.covariance[0]   = 10000.0;
    //     HectorSlamPoseData.pose.covariance[7]   = 10000.0;
    //     HectorSlamPoseData.pose.covariance[28]  = 10000.0;
    //     HectorSlamPoseData.pose.covariance[35]  = 10000.0;
    //     HectorSlamPoseData.twist.covariance[0]  = 10000.0;
    //     HectorSlamPoseData.twist.covariance[7]  = 10000.0;
    //     }

    PublishHectorSlamData();

}

void droneRobotLocalizationROSModule::droneSlamDunkPoseCallback(const geometry_msgs::PoseStamped &msg)
{

    ros::Time current_timestamp;

    SlamDunkPoseData.header.stamp = ros::Time::now();
    SlamDunkPoseData.child_frame_id = "fcu";
    SlamDunkPoseData.header.frame_id = "speeds_odom";

    SlamDunkPoseData.pose.pose.position.x = msg.pose.position.x+init_position_x;
    SlamDunkPoseData.pose.pose.position.y = msg.pose.position.y+init_position_y;
    SlamDunkPoseData.pose.pose.position.z = msg.pose.position.z;
    SlamDunkPoseData.pose.pose.orientation.x = msg.pose.orientation.x;
    SlamDunkPoseData.pose.pose.orientation.y = msg.pose.orientation.y;
    SlamDunkPoseData.pose.pose.orientation.z = msg.pose.orientation.z;
    SlamDunkPoseData.pose.pose.orientation.w = msg.pose.orientation.w;


#ifdef slamdunk_has_angle
    /* Calculating Roll, Pitch, Yaw */
    tf::Quaternion q(msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w);
    tf::Matrix3x3 m(q);

    //convert quaternion to euler angels
    double yaw, pitch, roll;
    m.getRPY(roll, pitch, yaw);

    //subtracting the pitch offset
    pitch = pitch - slamdunk_angle;

    //converting back to quaternions
    tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll,pitch,yaw);
    SlamDunkPoseData.pose.pose.orientation.x = quaternion.getX();
    SlamDunkPoseData.pose.pose.orientation.y = quaternion.getY();
    SlamDunkPoseData.pose.pose.orientation.z = quaternion.getZ();
    SlamDunkPoseData.pose.pose.orientation.w = quaternion.getW();

#endif

    current_timestamp = ros::Time::now();

    double x_raw_t = msg.pose.position.x;
    double y_raw_t = msg.pose.position.y;
    double z_raw_t = msg.pose.position.z;

    time_t tv_sec; suseconds_t tv_usec;
    {
        tv_sec  = current_timestamp.sec;
        tv_usec = current_timestamp.nsec / 1000.0;
        filtered_derivative_wcb_x.setInput( x_raw_t, tv_sec, tv_usec);
        filtered_derivative_wcb_y.setInput( y_raw_t, tv_sec, tv_usec);
        filtered_derivative_wcb_z.setInput( z_raw_t, tv_sec, tv_usec);
    }

    double x_t, dx_t;
    double y_t, dy_t;
    double z_t, dz_t;
    filtered_derivative_wcb_x.getOutput( x_t,  dx_t);
    filtered_derivative_wcb_y.getOutput( y_t,  dy_t);
    filtered_derivative_wcb_z.getOutput( z_t,  dz_t);

    double y, p, r;
    //     m.getEulerYPR(y, p, r);
    y = EstimatedPose.yaw;
    p = EstimatedPose.pitch;
    r = EstimatedPose.roll;

    Eigen::Vector3f BodyFrame;
    Eigen::Vector3f GlobalFrame;
    Eigen::Matrix3f RotationMat;

    GlobalFrame(0) = (+1)*dx_t;
    GlobalFrame(1) = (+1)*dy_t;
    GlobalFrame(2) = 0;

    RotationMat(0,0) = cos(y);
    RotationMat(1,0) = -sin(y);
    RotationMat(2,0) = 0;

    RotationMat(0,1) = sin(y);
    RotationMat(1,1) = cos(y);
    RotationMat(2,1) = 0;

    RotationMat(0,2) = 0;
    RotationMat(1,2) = 0;
    RotationMat(2,2) = 1;

    BodyFrame = RotationMat*GlobalFrame;


    SlamDunkPoseData.twist.twist.linear.x  = (+1) * BodyFrame(0);
    SlamDunkPoseData.twist.twist.linear.y  = (+1) * BodyFrame(1);
    SlamDunkPoseData.twist.twist.linear.z  = dz_t;

    SlamDunkPoseData.pose.covariance[0]   = 0.9;
    SlamDunkPoseData.pose.covariance[7]   = 0.9;
    //SlamDunkPoseData.pose.covariance[21]  = 0.9;
    //SlamDunkPoseData.pose.covariance[28]  = 0.9;
    //SlamDunkPoseData.pose.covariance[35]  = 0.9;
    //    SlamDunkPoseData.twist.covariance[0]  = 0.09;
    //    SlamDunkPoseData.twist.covariance[7]  = 0.09;

    PublishSlamdunkData();
}

void droneRobotLocalizationROSModule::droneVOSemanticSLAMPoseCallback(const geometry_msgs::PoseStamped& msg)
{
    VOSemanticSLAMPoseData.child_frame_id = "fcu";
    VOSemanticSLAMPoseData.header.frame_id = "speeds_odom";
    VOSemanticSLAMPoseData.header.stamp = ros::Time::now();

    VOSemanticSLAMPoseData.pose.pose.position.x = msg.pose.position.x;
    VOSemanticSLAMPoseData.pose.pose.position.y = msg.pose.position.y;
    VOSemanticSLAMPoseData.pose.pose.position.z = msg.pose.position.z;

    VOSemanticSLAMPoseData.pose.pose.orientation.x = msg.pose.orientation.x;
    VOSemanticSLAMPoseData.pose.pose.orientation.y = msg.pose.orientation.y;
    VOSemanticSLAMPoseData.pose.pose.orientation.z = msg.pose.orientation.z;
    VOSemanticSLAMPoseData.pose.pose.orientation.w = msg.pose.orientation.w;

    ros::Time current_timestamp = ros::Time::now();
    double x_raw_t = msg.pose.position.x;
    double y_raw_t = msg.pose.position.y;
    double z_raw_t = msg.pose.position.z;

    time_t tv_sec; suseconds_t tv_usec;
    {
        tv_sec  = current_timestamp.sec;
        tv_usec = current_timestamp.nsec / 1000.0;
        filtered_derivative_vos_x.setInput( x_raw_t, tv_sec, tv_usec);
        filtered_derivative_vos_y.setInput( y_raw_t, tv_sec, tv_usec);
        filtered_derivative_vos_z.setInput( z_raw_t, tv_sec, tv_usec);
    }

    double x_t, dx_t;
    double y_t, dy_t;
    double z_t, dz_t;
    filtered_derivative_vos_x.getOutput( x_t,  dx_t);
    filtered_derivative_vos_y.getOutput( y_t,  dy_t);
    filtered_derivative_vos_z.getOutput( z_t,  dz_t);

    double y, p, r;
    //     m.getEulerYPR(y, p, r);
    y = EstimatedPose.yaw;
    p = EstimatedPose.pitch;
    r = EstimatedPose.roll;

    Eigen::Vector3f BodyFrame;
    Eigen::Vector3f GlobalFrame;
    Eigen::Matrix3f RotationMat;

    GlobalFrame(0) = (+1)*dx_t;
    GlobalFrame(1) = (+1)*dy_t;
    GlobalFrame(2) = 0;

    RotationMat(0,0) = cos(y);
    RotationMat(1,0) = -sin(y);
    RotationMat(2,0) = 0;

    RotationMat(0,1) = sin(y);
    RotationMat(1,1) = cos(y);
    RotationMat(2,1) = 0;

    RotationMat(0,2) = 0;
    RotationMat(1,2) = 0;
    RotationMat(2,2) = 1;

    BodyFrame = RotationMat*GlobalFrame;


    VOSemanticSLAMPoseData.twist.twist.linear.x  = (+1) * BodyFrame(0);
    VOSemanticSLAMPoseData.twist.twist.linear.y  = (+1) * BodyFrame(1);
    VOSemanticSLAMPoseData.twist.twist.linear.z  = dz_t;


    VOSemanticSLAMPoseData.pose.covariance[0] = 0.9;
    VOSemanticSLAMPoseData.pose.covariance[7] = 0.9;
    VOSemanticSLAMPoseData.pose.covariance[21]  = 0.9;
    VOSemanticSLAMPoseData.pose.covariance[28]  = 0.9;
    VOSemanticSLAMPoseData.pose.covariance[35]  = 0.9;
    VOSemanticSLAMPoseData.twist.covariance[0]  = 0.09;
    VOSemanticSLAMPoseData.twist.covariance[7]  = 0.09;
    VOSemanticSLAMPoseData.twist.covariance[35] = 0.09;

    PublishVOSemanticSLAMPoseData();

}

void droneRobotLocalizationROSModule::droneLaserScanPoseCallback(const nav_msgs::Odometry &msg)
{
    LaserScanPoseData.child_frame_id  = "fcu";
    LaserScanPoseData.header.frame_id = "speeds_odom";
    LaserScanPoseData.header.stamp    =  ros::Time::now();

    ros::Time current_timestamp = ros::Time::now();

    double x_raw_t = msg.pose.pose.position.x;
    double y_raw_t = msg.pose.pose.position.y;

    time_t tv_sec; suseconds_t tv_usec;
    {
        tv_sec  = current_timestamp.sec;
        tv_usec = current_timestamp.nsec / 1000.0;
        filtered_derivative_wcb_lx.setInput( x_raw_t, tv_sec, tv_usec);
        filtered_derivative_wcb_ly.setInput( y_raw_t, tv_sec, tv_usec);
    }

    double x_t, dx_t;
    double y_t, dy_t;
    filtered_derivative_wcb_lx.getOutput( x_t,  dx_t);
    filtered_derivative_wcb_ly.getOutput( y_t,  dy_t);


    LaserScanPoseData.pose.pose.position.x  = msg.pose.pose.position.x ;  /*PoseIn.pose.position.x */;
    LaserScanPoseData.pose.pose.position.y  = msg.pose.pose.position.y ;  /*PoseIn.pose.position.y*/;
    LaserScanPoseData.pose.pose.orientation = msg.pose.pose.orientation;


    //convert quaternion to euler angels
    double y, p, r;
    //     m.getEulerYPR(y, p, r);
    y = EstimatedPose.yaw;
    p = EstimatedPose.pitch;
    r = EstimatedPose.roll;

    Eigen::Vector3f BodyFrame;
    Eigen::Vector3f GlobalFrame;
    Eigen::Matrix3f RotationMat;

    GlobalFrame(0) = (+1)*dx_t;
    GlobalFrame(1) = (+1)*dy_t;
    GlobalFrame(2) = 0;

    RotationMat(0,0) = cos(y);
    RotationMat(1,0) = -sin(y);
    RotationMat(2,0) = 0;

    RotationMat(0,1) = sin(y);
    RotationMat(1,1) = cos(y);
    RotationMat(2,1) = 0;

    RotationMat(0,2) = 0;
    RotationMat(1,2) = 0;
    RotationMat(2,2) = 1;

    BodyFrame = RotationMat*GlobalFrame;

    LaserScanPoseData.twist.twist.linear.x  = (+1) * BodyFrame(0);
    LaserScanPoseData.twist.twist.linear.y  = (+1) * BodyFrame(1);

    LaserScanPoseData.pose.covariance[0]   = 5.0;
    LaserScanPoseData.pose.covariance[7]   = 5.0;
    LaserScanPoseData.pose.covariance[28]  = 5.0;
    LaserScanPoseData.pose.covariance[35]  = 5.0;
    LaserScanPoseData.twist.covariance[0]  = 5.0;
    LaserScanPoseData.twist.covariance[7]  = 5.0;

    PublishLaserScanPoseData();

}

void droneRobotLocalizationROSModule::droneLaserScanWindowPoseCallback(const nav_msgs::Odometry &msg)
{
    LaserScanWindowPoseData.child_frame_id  = "fcu";
    LaserScanWindowPoseData.header.frame_id = "speeds_odom";
    LaserScanWindowPoseData.header.stamp    =  ros::Time::now();

    ros::Time current_timestamp = ros::Time::now();

    double x_raw_t = msg.pose.pose.position.x;
    double y_raw_t = msg.pose.pose.position.y;

    time_t tv_sec; suseconds_t tv_usec;
    {
        tv_sec  = current_timestamp.sec;
        tv_usec = current_timestamp.nsec / 1000.0;
        filtered_derivative_wcb_lwx.setInput( x_raw_t, tv_sec, tv_usec);
        filtered_derivative_wcb_lwy.setInput( y_raw_t, tv_sec, tv_usec);
    }

    double x_t, dx_t;
    double y_t, dy_t;
    filtered_derivative_wcb_lwx.getOutput( x_t,  dx_t);
    filtered_derivative_wcb_lwy.getOutput( y_t,  dy_t);


    LaserScanWindowPoseData.pose.pose.position.x  = msg.pose.pose.position.x ;  /*PoseIn.pose.position.x */;
    LaserScanWindowPoseData.pose.pose.position.y  = msg.pose.pose.position.y ;  /*PoseIn.pose.position.y*/;
    LaserScanWindowPoseData.pose.pose.orientation = msg.pose.pose.orientation;


    //convert quaternion to euler angels
    double y, p, r;
    //     m.getEulerYPR(y, p, r);
    y = EstimatedPose.yaw;
    p = EstimatedPose.pitch;
    r = EstimatedPose.roll;

    Eigen::Vector3f BodyFrame;
    Eigen::Vector3f GlobalFrame;
    Eigen::Matrix3f RotationMat;

    GlobalFrame(0) = (+1)*dx_t;
    GlobalFrame(1) = (+1)*dy_t;
    GlobalFrame(2) = 0;

    RotationMat(0,0) = cos(y);
    RotationMat(1,0) = -sin(y);
    RotationMat(2,0) = 0;

    RotationMat(0,1) = sin(y);
    RotationMat(1,1) = cos(y);
    RotationMat(2,1) = 0;

    RotationMat(0,2) = 0;
    RotationMat(1,2) = 0;
    RotationMat(2,2) = 1;

    BodyFrame = RotationMat*GlobalFrame;

    LaserScanWindowPoseData.twist.twist.linear.x  = (+1) * BodyFrame(0);
    LaserScanWindowPoseData.twist.twist.linear.y  = (+1) * BodyFrame(1);

    LaserScanWindowPoseData.pose.covariance[0]   = 1;
    LaserScanWindowPoseData.pose.covariance[7]   = 1;
    LaserScanWindowPoseData.pose.covariance[28]  = 1;
    LaserScanWindowPoseData.pose.covariance[35]  = 1;
    LaserScanWindowPoseData.twist.covariance[0]  = 1;
    LaserScanWindowPoseData.twist.covariance[7]  = 1;

    PublishLaserScanWindowPoseData();

}

void droneRobotLocalizationROSModule::droneOdometryFilteredCallback(const nav_msgs::Odometry &msg)
{
    //-----------------------------------------------------------------------------------------------------------
    // Estimated pose from the EKF
    //-----------------------------------------------------------------------------------------------------------


    EstimatedPose.x = msg.pose.pose.position.x;
    EstimatedPose.y = msg.pose.pose.position.y;
    EstimatedPose.z = msg.pose.pose.position.z;

    //converting the orientation from quaternion to roll, pitch and yaw
    tf::Quaternion q(msg.pose.pose.orientation.x,msg.pose.pose.orientation.y,
                     msg.pose.pose.orientation.z, msg.pose.pose.orientation.w);

    tf::Matrix3x3 m(q);
    double yaw, pitch, roll;
    m.getEulerYPR(yaw, pitch, roll);

    EstimatedPose.pitch = pitch;
    EstimatedPose.roll  = roll;
    EstimatedPose.yaw   = yaw;

    //-------------------------------------------------------------------------------------------------------------
    // Estimated Speeds from the EKF
    //-------------------------------------------------------------------------------------------------------------

    //transformVector funtion requires the frame_id as well as the current time
    vin.header.stamp = ros::Time::now();
    vin.header.frame_id = "/base_link";
    vin.vector.x = msg.twist.twist.linear.x;
    vin.vector.y = msg.twist.twist.linear.y;
    vin.vector.z = msg.twist.twist.linear.z;

    //Using tf to convert the speeds (twist.linear) messages from base_link frame to odom frame as
    //the Aerostack requires speeds in odom frame (i.e world frame)
    try{
        listener.transformVector("/odom",ros::Time(0) ,vin, "/base_link",vout);
    }
    catch (tf::TransformException ex)
    {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
    }

    EstimatedSpeeds.dx   =  vout.vector.x;
    EstimatedSpeeds.dy   =  vout.vector.y;
    EstimatedSpeeds.dz   = -vout.vector.z;
    EstimatedSpeeds.dyaw = -msg.twist.twist.angular.z;

    PublishEstimatedData();
    return;
}

void droneRobotLocalizationROSModule::droneBebopCallback(const bebop_msgs::Ardrone3PilotingStateAttitudeChangedConstPtr &attimsg,
                                                         const bebop_msgs::Ardrone3PilotingStateAltitudeChangedConstPtr &altimsg,
                                                         const bebop_msgs::Ardrone3PilotingStateSpeedChangedConstPtr &speedsmsg)
{
    //    ros::Time current_time = ros::Time::now();
    //    bebop_imu_data.header.stamp = current_time;
    //    bebop_imu_data.header.frame_id = "fcu";

    //    bebop_vel_alti_data.header.stamp = current_time;
    //    bebop_vel_alti_data.header.frame_id = "speeds_odom";
    //    bebop_vel_alti_data.child_frame_id  = "fcu";

    //    //convert roll, pitch and yaw to required frame and quaternions
    //    float roll, pitch, yaw;

    //    //converting the angles from NED to ENU
    //    roll  = attimsg->roll;
    //    pitch = -attimsg->pitch;
    //    yaw   = -attimsg->yaw;

    //    tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll,pitch,yaw);
    //    bebop_imu_data.orientation.x = quaternion.getX();
    //    bebop_imu_data.orientation.y = quaternion.getY();
    //    bebop_imu_data.orientation.z = quaternion.getZ();
    //    bebop_imu_data.orientation.w = quaternion.getW();

    //    //add speeds and converting it in ENU frame
    //    bebop_vel_alti_data.twist.twist.linear.x = speedsmsg->speedX;
    //    bebop_vel_alti_data.twist.twist.linear.y = -speedsmsg->speedY;
    //    bebop_vel_alti_data.twist.twist.linear.z = speedsmsg->speedZ;

    //    //add altitude
    //    bebop_vel_alti_data.pose.pose.position.z = altimsg->altitude;

    //PublishBebopData();

    return;
}

void droneRobotLocalizationROSModule::droneBebopOdomCallback(const nav_msgs::Odometry &msg)
{
    ros::Time current_time = ros::Time::now();
    bebop_imu_data.header.stamp = current_time;
    bebop_imu_data.header.frame_id = "fcu";

    bebop_odom_data.header.stamp = current_time;
    bebop_odom_data.header.frame_id = "speeds_odom";
    bebop_odom_data.child_frame_id  = "fcu";


    if(!bebop_first_yaw_measurement_)
    {
        tf::Quaternion q(msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w);
        tf::Matrix3x3 m(q);

        //convert quaternion to euler angels
        m.getRPY(bebop_first_roll_, bebop_first_pitch_, bebop_first_yaw_);
        //std::cout << "yaw: " << bebop_first_yaw_ << std::endl;

        bebop_first_yaw_measurement_ = true;
    }

    //converting the bebop odom data to the one required by aerostack frame
    Eigen::Vector3f bebop_frame;
    Eigen::Vector3f aerostack_frame;
    Eigen::Matrix3f RotationMat;

    std::cout << "yaw: " << bebop_first_yaw_ << std::endl;

    bebop_frame(0) = msg.pose.pose.position.x;
    bebop_frame(1) = msg.pose.pose.position.y;
    bebop_frame(2) = msg.pose.pose.position.z;

    RotationMat(0,0) = cos(bebop_first_yaw_);
    RotationMat(1,0) = -sin(bebop_first_yaw_);
    RotationMat(2,0) = 0;

    RotationMat(0,1) = sin(bebop_first_yaw_);
    RotationMat(1,1) = cos(bebop_first_yaw_);
    RotationMat(2,1) = 0;

    RotationMat(0,2) = 0;
    RotationMat(1,2) = 0;
    RotationMat(2,2) = 1;

    aerostack_frame = RotationMat * bebop_frame;

    bebop_odom_data.pose.pose.position.x = aerostack_frame(0);
    bebop_odom_data.pose.pose.position.y = aerostack_frame(1);
    bebop_odom_data.pose.pose.position.z = aerostack_frame(2);


    bebop_odom_data.twist = msg.twist;

    //IMU data
    double roll, pitch, yaw;
    tf::Quaternion q1(msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w);
    tf::Matrix3x3 m1(q1);

    //convert quaternion to euler angels
    m1.getRPY(roll, pitch, yaw);
    yaw = yaw - bebop_first_yaw_;

    tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll,pitch,yaw);
    bebop_imu_data.orientation.x = quaternion.getX();
    bebop_imu_data.orientation.y = quaternion.getY();
    bebop_imu_data.orientation.z = quaternion.getZ();
    bebop_imu_data.orientation.w = quaternion.getW();

    PublishBebopIMUData();
    PublishBebopOdomData();

}

void droneRobotLocalizationROSModule::PublishImuData()
{
    if(moduleStarted == true)
        droneImuPub.publish(Imu_data);
    return;
}

void droneRobotLocalizationROSModule::PublishRotationAnglesData()
{
    if(moduleStarted == true)
        droneRotationAnglesPub.publish(rotationAngleData);
    return;
}

void droneRobotLocalizationROSModule::PublishOdomData()
{
    //Publishing the Controller command data to the ekf
    controllerOdometryData.header.stamp = ros::Time::now();
    controllerOdometryData.header.frame_id = "speeds_odom";
    controllerOdometryData.child_frame_id  = "fcu";

    controllerOdometryData.twist.twist.linear.x  = -PitchRollCmd.pitchCmd;
    controllerOdometryData.twist.twist.linear.y  = -PitchRollCmd.rollCmd;
    controllerOdometryData.twist.twist.linear.z  = DaltitudeCmd.dAltitudeCmd;
    controllerOdometryData.twist.twist.angular.z = +DyawCmd.dYawCmd;
    controllerOdometryData.twist.covariance[0]  = pitch_cmd_co;
    controllerOdometryData.twist.covariance[7]  = roll_cmd_co;
    controllerOdometryData.twist.covariance[14] = daltitude_cmd_co;
    controllerOdometryData.twist.covariance[29] = dyaw_cmd_co;

    if(moduleStarted == true)
        droneOdomPub.publish(controllerOdometryData);
    return;
}

void droneRobotLocalizationROSModule::PublishAltitudeData()
{
    if(moduleStarted == true)
        droneAltitudePub.publish(altitudeData);
    return;
}

void droneRobotLocalizationROSModule::PublishAltitudeGTData()
{
    if(moduleStarted == true)
        droneAltitudeGTPub.publish(altitudeGTData);
    return;
}

void droneRobotLocalizationROSModule::PublishSpeedsData()
{
    if(moduleStarted == true)
        droneGroundSpeedsPub.publish(groundSpeedsData);
    return;
}

void droneRobotLocalizationROSModule::PublishPnPData()
{
    if(moduleStarted == true)
        dronePnPPosePub.publish(PnPPoseData);
    return;

}

void droneRobotLocalizationROSModule::PublishGPSData()
{
    if(moduleStarted == true)
        droneGPSdataPub.publish(gps_data);
    return;

}

void droneRobotLocalizationROSModule::PublishHectorSlamData()
{
    if(moduleStarted == true)
        droneHectorSlamDataPub.publish(HectorSlamPoseData);
    return;
}

void droneRobotLocalizationROSModule::PublishSlamdunkData()
{
    if(moduleStarted == true)
        droneSlamDunkPosePub.publish(SlamDunkPoseData);
    return;
}

void droneRobotLocalizationROSModule::PublishVOSemanticSLAMPoseData()
{
    if(moduleStarted == true)
        droneVOSematicSLAMPub.publish(VOSemanticSLAMPoseData);
    return;

}

void droneRobotLocalizationROSModule::PublishLaserScanPoseData()
{
    if(moduleStarted == true)
        droneLaserScanPosePub.publish(LaserScanPoseData);
    return;
}

void droneRobotLocalizationROSModule::PublishLaserScanWindowPoseData()
{
    if(moduleStarted == true)
        droneLaserScanWindowPosePub.publish(LaserScanWindowPoseData);
    return;
}

void droneRobotLocalizationROSModule::PublishEstimatedData()
{
    if(moduleStarted == true)
    {
        droneEstimatedPosePub.publish(EstimatedPose);
        droneEstimatedSpeedsPub.publish(EstimatedSpeeds);
    }
    return;
}

void droneRobotLocalizationROSModule::PublishBebopIMUData()
{
    if(moduleStarted == true)
    {
        droneBebopIMUPub.publish(bebop_imu_data);
        //dronebebopVelandAltiPub.publish(bebop_vel_alti_data);
    }
    return;
}

void droneRobotLocalizationROSModule::PublishBebopOdomData()
{
    if(moduleStarted == true)
    {
        droneBebopOdomDataPub.publish(bebop_odom_data);
    }

}
