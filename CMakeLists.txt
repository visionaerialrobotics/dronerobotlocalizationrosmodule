cmake_minimum_required(VERSION 2.4.6)

set(PROJECT_NAME droneRobotLocalizationROSModule)
project(${PROJECT_NAME})

#include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)


### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
#add_definitions(-std=c++03)




# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)



set(DRONE_ROBOT_LOCALIZATION_ROS_MODULE_SOURCE_DIR
	src/sources)
	
set(DRONE_ROBOT_LOCALIZATION_ROS_MODULE_INCLUDE_DIR
	src/include
	)

set(DRONE_ROBOT_LOCALIZATION_ROS_MODULE_SOURCE_FILES
	${DRONE_ROBOT_LOCALIZATION_ROS_MODULE_SOURCE_DIR}/droneRobotLocalizationROSModule.cpp

	
	)
	
set(DRONE_ROBOT_LOCALIZATION_ROS_MODULE_HEADER_FILES
	${DRONE_ROBOT_LOCALIZATION_ROS_MODULE_INCLUDE_DIR}/droneRobotLocalizationROSModule.h

	)
	

#Nodes
set(DRONE_ROBOT_LOCALIZATION_ROS_MODULE_NODE_SOURCE_FILES
    src/sources/droneRobotLocalizationROSModuleNode.cpp
)
	


find_package(catkin REQUIRED
                COMPONENTS roscpp droneModuleROS droneMsgsROS tf tf_conversions lib_pugixml lib_cvgutils bebop_msgs
)

#if OpenCV
#find_package(OpenCV REQUIRED)


catkin_package(
        CATKIN_DEPENDS roscpp droneModuleROS droneMsgsROS tf tf_conversions lib_pugixml lib_cvgutils bebop_msgs
  )


include_directories(${catkin_INCLUDE_DIRS})



#NODES
include_directories(${DRONE_ROBOT_LOCALIZATION_ROS_MODULE_INCLUDE_DIR})
add_executable(droneRobotLocalizationROSModuleNode ${DRONE_ROBOT_LOCALIZATION_ROS_MODULE_NODE_SOURCE_FILES} ${DRONE_ROBOT_LOCALIZATION_ROS_MODULE_SOURCE_FILES} ${DRONE_ROBOT_LOCALIZATION_ROS_MODULE_HEADER_FILES})

#external libraries, like OpenCV
#target_link_libraries(droneExamplePackageNode ${OpenCV_LIBS})

target_link_libraries(droneRobotLocalizationROSModuleNode ${catkin_LIBRARIES})



set(OTHER_FILES
        package.xml
)
add_custom_target(other_files SOURCES ${OTHER_FILES})


